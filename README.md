# securityContext-demo

Demo project

Steps:
1. Clone or download the sample application.
    Please go inside the project.  [cd securityContext-demo]

2. To build the application:
    mvn clean install

Note: once, build got success **navigate to service/target**

3. To run:
    java -jar securityContextDemo-bootable.jar -Djboss.socket.binding.port-offset=12000 -Dmp.security.domain=other

Once, jar file gets executed Please test with below API to see the response.

4. scurl -s -k -X GET -H "Content-Type: text/plain" http://localhost:20080/demo/example/hello.


