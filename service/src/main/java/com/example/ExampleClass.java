package com.exmaple;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.GET;
import org.jboss.logging.Logger;

@Path("/hello")
public class ExampleClass
{
    @Context
    private static Logger log = Logger.getLogger(ExampleClass.class.getName());

    @GET
    @Produces("text/plain")
    public String whoIsThis()
    {
        return "sample";
    }
}
